# File: /example/app.py
# Project: gtklara
# File Created: 18-11-2023 15:39:42
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2022 - 2023
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from gtklara import GTKlara, State
from .views import Home
from .fragments import ComboBox, Entry, Radio, Bool, Label, DoubleC, Button


class App(GTKlara):
    def __init__(self, children=[], **kwargs):
        super().__init__(children, kwargs)

    entry_value = State("hello")
    cb_value = State(
        lambda v: print(v),
    )

    async def on_hello(self, widget):
        print("hello")

    def render(self):
        return Home(
            [
                DoubleC(
                    [
                        Label(label=self.entry_value),
                        Bool(
                            on_changed=lambda v: print(v),
                        ),
                        Radio(
                            options={"hello": "world", "howdy": "texas"},
                            on_changed=lambda v: print(v),
                        ),
                        Button(on_clicked=self.cb_value),
                    ]
                ),
                Entry(
                    title=self.entry_value,
                    on_changed=lambda w: self.entry_value.set(w.get_text()),
                ),
                ComboBox(
                    items={"hello": "world", "howdy": "texas"}, on_changed=self.cb_value
                ),
            ],
            on_clicked=self.on_hello,
        )
